var mongoose = require('mongoose');
var conf = require('../config');
mongoose.connect(conf.mongodb.url, conf.mongodb.options);
var User = require('../app/models/user');
User.create(
    {
        name: 'admin',
        password: 'admin',
        role: 'admin',
        email: 'admin@productivity.com'
    }, function (err) {
            if (err) {
                console.log(err);
            }
            process.exit();
    }
);