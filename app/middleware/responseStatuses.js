module.exports = function (){
    return function(req, res, next) {
        res.ok = function (data) {
            return res.status(200).json(data);
        };
        res.badRequest = function (details) {
            return res.status(400).json({"error": "Bad request", "details": details})
        };
        res.invalidInput = function(details) {
            return res.status(401).json({"error": "Invalid input", "details": details});
        };
        res.invalidData = function (details) {
            return res.status(409).json({"error": "Conflict", "details": details});
        };
        next();
    };
};