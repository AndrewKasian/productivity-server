module.exports = function (req, res, next) {
    if(req.user.role === 'admin'){
        next();
    }else{
        res.badRequest('User is not an admin');
    }
};