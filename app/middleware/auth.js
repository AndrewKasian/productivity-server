var jwt = require('jsonwebtoken');
module.exports = function (req, res, next) {
    jwt.verify(req.header('Token'), 'SECRET', function (err, user) {
        if(err || !user){
            return res.badRequest('User is not authorized');
        }
        req.user = user;
        next();
    });
};