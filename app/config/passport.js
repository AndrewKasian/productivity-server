var LocalStrategy  = require('passport-local').Strategy;
var User = require('../models/user');

module.exports = function (passport) {
    passport.serializeUser(function(user, done) {
        done(null, user.id);
    });

    passport.deserializeUser(function(id, done) {
        User.findById(id, function(err, user) {
            done(err, user);
        });
    });

    passport.use(new LocalStrategy({
        usernameField: 'email',
        passwordField: 'password'
    },function (email, password, done) {
        User.findOne({email: email}, function (err, user) {
            if (err) {
                return done(null, false, { message: 'Database error' });
            }

            if (!user) {
                return done(null, false, { message: 'Incorrect email.' });
            }

            if (!user.checkUser(password)) {
                return done(null, false, { message: 'Incorrect password.' });
            }

            return done(null, user);
        })
    }));
};


