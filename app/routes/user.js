module.exports = function (router) {
    'use strict';

    var User = require('../models/user');
    var Task = require('../models/task');
    var auth = require('../middleware/auth');
    var isAdmin = require('../middleware/isAdmin');

    /**
     * Role - Admin
     * GEt /user  => get all Users
     * POST /user => create new User
     * PUT /user/:id => update User
     * DELETE /user/:id => delete User
     */

    //ADMIN - in progress
    router.route('/user/:id')
        //GET All Users
        .get(auth, isAdmin, function(req, res) {
            User.find(function(err, users) {
                if (err || !users){
                    return res.badRequest(err.message);
                }
                res.ok(users);
            });
        })
        //CREATE new User
        .post(auth, isAdmin, function(req, res) {
            if (!req.body.name || !req.body.password || !req.body.email) {
                return res.badRequest('Please fill out all fields');
            }
            var user = new User();
            user.name = req.body.name;
            user.email = req.body.email;
            user.password = req.body.password;
            user.save(function(err) {
                if (err){
                    if(err.code === 11000){
                        return res.invalidData('Email already exist');
                    }
                    return res.badRequest(err.message);
                }
                return res.ok({token:user.generateJWT()});
            });
        })
        //UPDATE User
        .put(auth, isAdmin, function(req, res) {
            User.findById(req.params.id, function(err, user) {
                if (err || !user){
                    return  res.badRequest(err.message);
                }
                user.name = req.body.name;
                user.password = req.body.password;
                user.save(function(err) {
                    if (err){
                        return res.badRequest(err.message);
                    }
                    res.ok({ message: 'User updated!' });
                });

            });
        })
        //DELETE User
        //TODO update status to deleted
        .delete(auth, isAdmin, function(req, res) {
            User.remove({
                _id: req.params.id
            }, function(err, user) {
                if (err && !user){
                    return res.badRequest(err.message);
                }
                res.ok({ message: 'User deleted' });
            });
        });

    /**
     * USER
     * GEt /profile  => get current auth Users
     * PUT /profile => update current auth User
     */

    //USER
    router.route('/profile')
        .get(auth, function (req, res) {
            User.findByEmail(req.user.email, function(err, user) {
                if (err || !user) {
                    return res.badRequest(err.message);
                }
                Task.count({_owner: user._id})
                    .exec(function (err, count) {
                        if (err) {
                            return res.badRequest(err.message);
                        }
                        user = user.toJSON();
                        user.tasksAmount = count;
                        res.ok(user);
                    });
            });
        })
        //UPDATE User
        .put(auth, function(req, res) {
            User.findByEmail(req.user.email, function(err, user) {
                if (err || !user){
                    return  res.badRequest(err.message);
                }
                user.name = req.body.name;
                user.password = req.body.password;
                user.save(function(err) {
                    if (err){
                        return res.badRequest(err.message);
                    }
                    res.ok({ message: 'User updated!' });
                });
            });
        });
};

