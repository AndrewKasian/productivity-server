module.exports = function (router) {
    'use strict';

    var User = require('../models/user');
    var Task = require('../models/task');
    var auth = require('../middleware/auth');

    /**
     * GET /task => get auth User tasks
     * POST /task => create new Task for auth User
     * PUT /task => update task for auth User
     * DELETE /task => delete task for auth User
     */
    //Auth User tasks
    router.route('/task')
        //GET User tasks
        .get(auth,function (req, res) {
            User.findByEmail(req.user.email, function(err, user) {
                if (err || !user) {
                    return res.badRequest(err.message);
                }
                Task.find({_owner: user._id})
                    .exec(function (err, tasks) {
                        if(err || !tasks){
                            return res.badRequest(err.message)
                        }
                        res.ok(tasks);
                    });
            });
        })
        //CREATE new Task
        .post(auth, function(req, res) {
            User.findByEmail(req.user.email, function(err, user) {
                if (err || !user) {
                    return res.badRequest(err.message);
                }
                var task = new Task({
                    title: req.body.title,
                    _owner: user._id,   // assign the _id from the person
                    status: 'pending'
                });
                task.save(function (err) {
                    if (err){
                        return res.badRequest(err.message);
                    }
                    Task
                        .findById(task._id)
                        .populate('_owner')
                        .exec(function (err, task) {
                            if (err || !task){
                                return res.badRequest(err.message);
                            }
                            res.ok(task);
                        });
                });
            });
        })
        //UPDATE task
        .put(auth,function(req, res) {
            User.findByEmail(req.user.email, function(err, user) {
                if (err || !user) {
                    return res.badRequest(err.message);
                }
                Task.findById(req.body._id , function (err, task) {
                    if(err || !task){
                        return res.badRequest(err.message)
                    }
                    task.title = req.body.title;
                    task.status = req.body.status;
                    task.save(function(err) {
                        if (err){
                            return res.badRequest(err.message);
                        }
                        res.ok(task);
                    });
                })
            });
        })
        //DELETE User
        .delete(auth, function(req, res) {
            User.findByEmail(req.user.email, function(err, user) {
                if (err || !user) {
                    return res.badRequest(err.message);
                }
                Task.findOne({_owner: user._id} , function (err, task) {
                    if(err || !task){
                        return res.badRequest(err.message)
                    }
                    task.remove();
                    res.ok({ message: 'Task deleted' })
                })
            });
        });

};

