module.exports = function (router) {
    'use strict';

    /**
     * POST /registration => create new User
     * POST /login => check User and sign in
     */

    var User = require('../models/user');
    var passport = require('passport');

    /* POST Register a new user*/
    router.route('/registration')
        .post(function (req, res, next) {
            if (!req.body.name || !req.body.password || !req.body.email) {
                return res.badRequest('Please fill out all fields');
            }
            var user = new User();
            user.name = req.body.name;
            user.email = req.body.email;
            user.password = req.body.password;
            user.save(function(err) {
                if (err){
                    if(err.code === 11000){
                        return res.invalidData('Email already exist');
                    }
                    return res.badRequest(err.message);
                }
                return res.ok({token:user.generateJWT()});
            });
        });

    /*POST Login user created*/
    router.route('/login')
        .post(function (req, res, next) {
            if (!req.body.email){return res.badRequest('Please fill out email filed');}
            if (!req.body.password){return res.badRequest('Please fill out password field');}

            passport.authenticate('local', function(err, user, info){
                if (err) {
                    return res.badRequest(err.message);
                }

                if (!user) {
                    return res.invalidInput(info.message);
                }

                return res.ok({token:user.generateJWT()});
            })(req,res,next);
        });
};

