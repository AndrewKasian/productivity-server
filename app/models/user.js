var mongoose = require('mongoose');
var jwt = require('jsonwebtoken');
var crypto = require('crypto');
var Schema = mongoose.Schema;

//TODO new schema value "status"
var UserSchema = Schema({
    name    : {
        type: String,
        required: true
    },
    email : {
        type: String,
        require: true,
        unique: true
    },
    hash: {
        type: String,
        require: true
    },
    role: {
        type: String,
        default: 'user',
        require: true
    },
    salt: {
        type: String,
        require: true
    },
    iteration: {
        type: Number,
        require: true
    },
    __createdAt: {
        type: Date,
        default: Date.now()
    },
    tasks : [{ type: Schema.Types.ObjectId, ref: 'Task' }]
});

UserSchema.virtual('password')
    .set(function (password) {
        this.salt = String(Math.random());
        this.iteration = parseInt(Math.random()*10+1);
        this.hash = this.getHash(password);
    })
    .get(function () {
        return this.hash;
    });

UserSchema.methods.getHash = function (password) {
    var hash = crypto.createHmac('sha1', this.salt);
    for(var i = 0; i < this.iteration; i++){
        hash = hash.update(password);
    }
    return hash.digest('hex');
};

UserSchema.methods.checkUser = function (data) {
    return this.getHash(data) === this.hash;
};

UserSchema.methods.generateJWT = function(){
    //expiration one day
    var today = new Date();
    var exp = new Date(today);
    exp.setDate(today.getDate() + 1);

    return jwt.sign({
        name: this.name,
        email: this.email,
        role: this.role,
        exp: parseInt(exp.getTime() / 1000)
    }, 'SECRET');
};

UserSchema.methods.toJSON = function() {
    var userObj = this.toObject();
    delete userObj.hash;
    delete userObj.iteration;
    delete userObj.salt;
    delete userObj.__createdAt;
    return userObj
};

UserSchema.statics.findByEmail = function (email, callback) {
    return this.findOne({email: email}, callback);
};

module.exports = mongoose.model('User', UserSchema);