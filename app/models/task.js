var mongoose = require('mongoose');
var Schema   = mongoose.Schema;

var TasksSchema = new Schema({
    _owner: { type: String, ref: 'User' },
    title: {
        type: String,
        required: true
    },
    status: {
        type: String,
        required: true
    }
});

module.exports = mongoose.model('Task', TasksSchema);