var express = require('express');
var bodyParser = require('body-parser');
var mongoose = require('mongoose');
var passport = require('passport');
var cookieParser = require('cookie-parser');
var cors = require('./app/middleware/cors');
var resStatuses = require('./app/middleware/responseStatuses');
var conf = require('./config');
var app = express();
var router = express.Router();

mongoose.connect(conf.mongodb.url, conf.mongodb.options);

//Routes
require('./app/routes/auth')(router);
require('./app/routes/user')(router);
require('./app/routes/task')(router);

//User passport
require('./app/config/passport')(passport);

router.use(function(req, res, next) {
    console.log('Something is happening.');
    next();
});

app.use(cookieParser());
app.use(bodyParser.json());
app.use(cors());
app.use(resStatuses());

//auth using passport
app.use(passport.initialize());

app.use('/api', router);


//----------START SERVER-------------
app.listen(conf.server.port);
console.log('Server running on port ' + conf.server.port);