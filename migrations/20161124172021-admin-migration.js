'use strict';

module.exports = {

  up: function (db, next) {

    db.collection('users').save({"name" : "admin",
      "hash" : "e9b424c1e4f9c406e1cbb2ef2dae25a230bda5b2",
      "iteration" : 3,
      "salt" : "0.9628013585492718",
      "email" : "admin@productivity.com",
      "tasks" : [ ],
      "__createdAt" : new Date(),
      "role" : "admin",
      "__v" : 0
    }, next);
  },

  down: function (db, next) {
    db.collection('users').deleteOne({"name" : "admin"}, {}, next)
  }

};