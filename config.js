'use strict';

var config = {};

//Server settings
config.server = {};
config.server.port = process.env.PORT || 8080;

//MongoDB
config.mongodb = {};
config.changelogCollectionName = 'changelog';
config.mongodb.url = process.env.MONGO_URI || 'mongodb://127.0.0.1:27017/productivity';
config.mongodb.options = {};
config.mongodb.options.user = process.env.MONGO_USER || 'admin';
config.mongodb.options.pass = process.env.MONGO_PASS || 'admin';

module.exports = config;